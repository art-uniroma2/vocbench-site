/*
===============================================================================
Chili is the jQuery code highlighter plugin
...............................................................................
LICENSE: http://www.opensource.org/licenses/mit-license.php
WEBSITE: http://noteslog.com/chili/

											   Copyright 2008 / Andrea Ercolino
===============================================================================
*/

{
	  _name: "pearl_example"
	, _case: true
	, _main: {
		  mlcom  : { 
			  _match: /\/\*[^*]*\*+(?:[^\/][^*]*\*+)*\// 
			, _style: "color: #4040c2;"
		}
		, com    : { 
			  _match: /\s\/\/.*/ 
			, _style: "color: green;"
		}
		, string : { 
			  _match: /(?:\'[^\'\\\n]*(?:\\.[^\'\\\n]*)*\')|(?:\"[^\"\\\n]*(?:\\.[^\"\\\n]*)*\")/ 
			, _style: "color: teal;"
		}
		, meta   : { 
			  _match: /(?!\@interface\b)\@[\$\w]+\b/ 
			, _style: "color: red;"
		}
		, keyword: { 
			  _match: /\b(uri|literal\(.*\)|plain|rule|OPTIONAL|imports|dependsOn|last|alias|nodes|if|elseif|else|graph|where|parameters|\sid)\b/ 
			, _style: "color: red; font-weight: bold;"
		}
		, equalSymbol: {
				_match: /(\{|\})/
			,	_style: "color: blue; font-weight: bold;"
		}
		, typeFeat: {
				_match: /\_.*/
			,	_style: "color: blue; font-weight: bold;"
		}
		, placeholder: {
				_match: /\$\S*/
			,	_style: "color: brown; font-weight: bold;"
		}
		, var: {
				_match: /\?\S*/
			,	_style: "color: brown; font-weight: bold;"
		}
	}
}
