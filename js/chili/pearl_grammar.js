/*
===============================================================================
Chili is the jQuery code highlighter plugin
...............................................................................
LICENSE: http://www.opensource.org/licenses/mit-license.php
WEBSITE: http://noteslog.com/chili/

											   Copyright 2008 / Andrea Ercolino
===============================================================================
*/

{
	  _name: "pearl_grammar"
	, _case: true
	, _main: {
		  mlcom  : { 
			  _match: /\/\*[^*]*\*+(?:[^\/][^*]*\*+)*\// 
			, _style: "color: #4040c2;"
		}
		, com    : { 
			  _match: /\s\/\/.*/ 
			, _style: "color: green;"
		}
		, string : { 
			  _match: /(?:\'[^\'\\\n]*(?:\\.[^\'\\\n]*)*\')|(?:\"[^\"\\\n]*(?:\\.[^\"\\\n]*)*\")/ 
			, _style: "color: teal;"
		}
		, meta   : { 
			  _match: /(?!\@interface\b)\@[\$\w]+\b/ 
			, _style: "color: red;"
		}
		, keyword: { 
			  _match: /\b(:=|prRules|prefixDeclaration|prRule|prefix|namespace|uimaTypePR|ID|idVal|Conf|depend|depends|DependType|idVal|alias|nodes|graph|where|parameters|singleAlias|idAlias|uimaTypeAndFeats|node|idNode|type|condIf|condValueAndUIMAType|condElseIf|condElse|condBool|OntoRes|triple|tripleSubj|triplePred|tripleObj|parameterNameValue|parameterName|parameterValue|\sid)\b/ 
			, _style: "color: red; font-weight: bold;"
		}
		, regexQuant: {
				_match: /(\*|\+|\?)/
			,	_style: "color: blue; font-weight: bold;"
		}
		, equalSymbol: {
				_match: /(\{|\})/
			,	_style: "color: blue; font-weight: bold;"
		}
		, typeFeat: {
				_match: /\_.*/
			,	_style: "color: blue; font-weight: bold;"
		}
		, placeholder: {
				_match: /\$\S*/
			,	_style: "color: brown; font-weight: bold;"
		}
		, var: {
				_match: /\?\S*/
			,	_style: "color: brown; font-weight: bold;"
		}
	}
}
