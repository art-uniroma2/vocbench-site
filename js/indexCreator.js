var mainHeaderTag = "h2";
var subHeaderTag = "h3";

document.addEventListener("DOMContentLoaded", function() {
    var elements = document.body.getElementsByTagName("*");

    // Prepare the anchors for the headers
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].tagName.toLowerCase() == mainHeaderTag || elements[i].tagName.toLowerCase() == subHeaderTag) {
            var header = elements[i].textContent;
            elements[i].textContent = "";
            var hAnchor = document.createElement("a");
            hAnchor.setAttribute("id", getAnchorName(header));
            hAnchor.textContent = header;
            elements[i].appendChild(hAnchor);
        }
    }

    // Create the support index structure
    var indexStruct = [];
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].tagName.toLowerCase() == mainHeaderTag) {
            var subHeaderArray = [];
            for (var j = i + 1; j < elements.length; j++) {
                if (elements[j].tagName.toLowerCase() == subHeaderTag) {
                    subHeaderArray.push(elements[j].children[0].textContent);
                } else if (elements[j].tagName.toLowerCase() == mainHeaderTag) {
                    break;
                }
            }
            var indexItem = { mainHeader: elements[i].children[0].textContent, subHeaderList: subHeaderArray };
            indexStruct.push(indexItem);
        }
    }

    // Create the index list
    if (indexStruct.length > 0) {
        var ulIndex = document.createElement("ul");
        for (var i = 0; i < indexStruct.length; i++) {
            var mainHeaderListItem = document.createElement("li");
            var mainHeaderAnchor = document.createElement("a");
            mainHeaderAnchor.setAttribute("href", "#" + getAnchorName(indexStruct[i].mainHeader));
            mainHeaderAnchor.textContent = indexStruct[i].mainHeader;
            mainHeaderListItem.appendChild(mainHeaderAnchor);
            if (indexStruct[i].subHeaderList.length > 0) {
                var ulInner = document.createElement("ul");
                for (var j = 0; j < indexStruct[i].subHeaderList.length; j++) {
                    var subHeaderListItem = document.createElement("li");
                    var subHeaderAnchor = document.createElement("a");
                    subHeaderAnchor.setAttribute("href", "#" + getAnchorName(indexStruct[i].subHeaderList[j]));
                    subHeaderAnchor.textContent = indexStruct[i].subHeaderList[j];
                    subHeaderListItem.appendChild(subHeaderAnchor);
                    ulInner.appendChild(subHeaderListItem);
                }
                mainHeaderListItem.appendChild(ulInner);
            }
            ulIndex.appendChild(mainHeaderListItem);
        }

        // Add the index list to "IndexDiv" in the HTML
        var indexDiv = document.getElementById("IndexDiv");
        var indexHeader = document.createElement("h2");
        indexHeader.textContent = "Index";
        indexDiv.appendChild(indexHeader);
        indexDiv.appendChild(ulIndex);
    }

    // Scroll to the anchor if specified in the URL
    if (window.location.hash) {
        setTimeout(function() {
            var targetAnchor = document.querySelector(window.location.hash);
            if (targetAnchor) {
                targetAnchor.scrollIntoView({ behavior: "smooth", block: "start" });
            }
        }, 100); // Adjust delay if necessary
    }
});

function getAnchorName(header) {
    return header.trim().toLowerCase().replace(/[^0-9a-z-]/g, "_");
}
