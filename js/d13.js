//ORIGINAL JS FILE FROM SITE: http://dense13.com/blogFiles/wp-content/themes/dense13/js/d13.js
//this one contains small modifications


// Load shared scripts
var sharedScripts = [
	'highlighter/sh_main.js',
	'highlighter/lang/sh_javascript.min.js',
	'highlighter/lang/sh_php.min.js',
	'highlighter/lang/sh_html.min.js',
	'highlighter/lang/sh_css.min.js',
	//'highlighter/langs/sh_htaccess.js',

	'imgViewer/thumbnailviewer.js'
	];
for (var i=0, l=sharedScripts.length ; i<l ; i++) {
	document.write("<script type='text/javascript' src='../js/" +
		sharedScripts[i] + "'></script>");
}

// Load JS plugins stylesheets
var sharedStyles = [
	'highlighter/sh_style.css',

	'imgViewer/thumbnailviewer.css'
	];
for (var i=0, l=sharedStyles.length ; i<l ; i++) {
	document.write("<link rel='stylesheet' href='../js/" +
		sharedStyles[i] + "' type='text/css' media='screen' />");
}

// No more calls to document.write after this
document.close();

// On DOMContentLoaded, call init
(function(i){var u=navigator.userAgent;var e=/*@cc_on!@*/false;var b=setTimeout;if(/webkit/i.test(u)){b(function(){var a=document.readyState;if(a=="loaded"||a=="complete"){i()}else{b(arguments.callee,10)}},10)}else if((/mozilla/i.test(u)&&!/(compati)/.test(u))||(/opera/i.test(u))){document.addEventListener("DOMContentLoaded",i,false)}else if(e){(function(){var t=document.createElement('doc:rdy');try{t.doScroll('left');i();t=null}catch(e){b(arguments.callee,0)}})()}else{window.onload=i}})(starred_init);


/**
* this is the original function which is invoked upon loading of the document
*/
function init() {
	if (!document.getElementsByTagName) return;

	// Start image viewer
	thumbnailviewer.init();

	// Process CODE elements
	var codes = document.getElementsByTagName("code");
	var title, elTitle, elPre;
	for (var i=0, l=codes.length ; i<l ; i++) {
		elPre = codes[i].parentNode;
		if (elPre.tagName != "PRE") continue;

		// Add captions to code blocks
		title = codes[i].title;
		if (title) {
			// Remove title from CODE
			codes[i].title = "";
			// Prepare caption DIV
			elTitle = document.createElement("div");
			elTitle.innerHTML = title;
			elTitle.className = "JScodeCaption";
			// Add class to PRE
			elPre.className += " JS";
			// Insert before the PRE
			elPre.parentNode.insertBefore(elTitle, elPre);
		}

		// Prepare for highlighting
		switch (codes[i].className) {
			case 'css':
			case 'html':
			case 'php':
				elPre.className += " sh_" + codes[i].className;
				break;
			case 'js':
				elPre.className += " sh_javascript";
				break;
		}
	}


	// Start highlighter
	sh_highlightDocument();
}



/**
* this is my one
*/
function starred_init() {
	if (!document.getElementsByTagName) return;

	// Start image viewer
	//thumbnailviewer.init();  //STARRED: commented because i don't need the thumbnail viewer

	// Process CODE elements
	var pres = document.getElementsByTagName("pre");
	var elPre;
	for (var i=0, l=pres.length ; i<l ; i++) {
		elPre = pres[i];
		if (elPre.tagName != "PRE") continue;

		// Prepare for highlighting
		switch (elPre.className) {
			case 'css':
			case 'html':
			case 'php':
				elPre.className += " sh_" + pres[i].className;
				break;
			case 'js':
				elPre.className += " sh_javascript";
				break;
		}
	}
	
	// Start highlighter
	sh_highlightDocument();

}