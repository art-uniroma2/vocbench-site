<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:ui="http://java.sun.com/jsf/facelets"
      xmlns:f="http://java.sun.com/jsf/core"
      xmlns:h="http://java.sun.com/jsf/html">
<head>
  <title><ui:insert name="title">Default title</ui:insert></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link href="../../styles/art.css" rel="stylesheet" type="text/css" />      
</head>

<body jsfc="ui:composition"  template="/WEB-INF/templates/main-template-FILLERS.xhtml" 
							 xmlns="http://www.w3.org/1999/xhtml"
                             xmlns:ui="http://java.sun.com/jsf/facelets"
                             xmlns:f="http://java.sun.com/jsf/core">

<ui:define name="mainContent">

<h1>Dataset Catalogs</h1>
<p>
    Dataset catalogs are remotely accessible directories of ontologies, thesauri, ..., datasets. Actually, catalogs greatly
    differ in the type of the managed datasets, the range of metadata made accessible and the offered functionalities.</p>
<p>
    VocBench does not commit to any specific catalog, and relies on the extension point <a href="http://semanticturkey.uniroma2.it/doc/sys/dataset_catalog_connector.jsf">DatasetCatalogConnector</a> to support diverse catalogs.
    Predefined extensions support the following catalogs:
</p>
<ul>
    <li><a href="https://lov.linkeddata.es/">Linked Open Vocabularies</a> (LOV): this is a catalog of vocabularies (OWL ontologies and RDFS schemas),
    which is curated by human editors to assure the quality of its content and accompanying metadata. Individual entries in LOV are associated with an
    URL to download a cached copy of the vocabulary, but no SPARQL endpoint is provided (since vocabularies are generally intended for the description
    of the content of other datasets and are thus made available as files).</li>
    <li><a href="https://lod-cloud.net/">The Linked Open Data Cloud</a>: this is the home of the repository behind the famous diagram depicting the datasets being published
    and interconnected using the <a href="https://www.w3.org/DesignIssues/LinkedData.html">Linked Data</a> best practices and made available as
    <a href="https://opendefinition.org/">open data</a>. Actually, the documentation of the catalog does not prescribe anything on the license, beyond
    stating that "Access of the entire dataset must be possible via RDF crawling, via an RDF dump, or via a SPARQL endpoint." (more about technical openness
    than legal concerns about reusability).
    </li>
    <li><a href="https://data.europa.eu">data.europa.eu</a>: The official portal for European data, providing access to a collection of open dataset metadata harvested from international, EU, national, regional, local and geo data portals. It replaces the EU Open Data Portal and the European Data Portal.
    </li>
    <li><a href="http://showvoc.uniroma2.it/">ShowVoc</a>: an open-source software for creating data portals. Initially developed in the context of the <a href="https://ec.europa.eu/isa2/">ISA<sup>2</sup></a> action <a href="https://ec.europa.eu/isa2/actions/overcoming-language-barriers">Public Multilingual Knowledge Management Infrastructure for the Digital Single Market (PMKI)</a>, ShowVoc can be used to set up data portals for specific institutions, companies, etc. ShowVoc is based on the RDF service platform <a href="http://semanticturkey.uniroma2.it/">Semantic Turkey</a>, which also supports VocBench. ShowVoc stores a copy of <a href="http://showvoc.uniroma2.it/doc/user/contributions_submission.jsf#stable_resource_contribution">stable contributions</a> in projects alike the ones used in VocBench. Moreover, ShowVoc adopts a dedicated policy to grant anyone access to <em>public datasets</em>, whereas in VocBench users must be <a href="projects_adm.jsf#project-users_management">assigned to projects</a>. Furthermore, faceted search is based on <a href="user/projects.jsf#project_facets">project facets</a>, both standard ones (e.g. <em>model</em> and <em>lexicalization model</em>) and custom ones (decided for each ShowVoc installation).
    </li>
    <li><a href="https://ontoportal.org/">OntoPortal</a>: an open-source software for creating data portals based on the code originally written for the <a href="https://bioportal.bioontology.org/">BioPortal</a> ontology repository. In addition to the latter, which has become a reference with respect to biomedical ontologies, other installations cover different domains, such agriculture and environment, which are addressed by <a href="http://agroportal.lirmm.fr/">AgroPortal</a> and <a href="https://ecoportal.lifewatch.eu/">EcoPortal</a>, respectively. VocBench provides a general configuration targeting any OntoPortal installation, together with one specific for EcoPortal.
    </li>
</ul>
<p>
    The figure below depicts the main dialog for the interaction with a dataset catalog. Its main function is to support the user to search a dataset
    matching some criteria. Currently, this functionality is exploited when "adding an import" or when "preloading a newly created project with some data".
</p>
<div class="figure"><img src="images/dataset-catalogs_dataset-search.png" alt="Dataset catalogs: dataset search"/></div>
<p>
    The topmost drop-down list, labeled "Catalog", supports the selection of the catalog to use. The text field just below, labeled "Search", can be used to
    enter a search string. To start a search, it is sufficient to hit <code>ENTER</code> (when the cursor is in the search text field) or to press the nearby
    button with the magnifier icon.
</p>
<p>
    The search results are listed below the input widgets, sorted by relevance (if returned by the catalog), with the most relevant result at the top.
    Results are paginated to prevent problems with very large result sets. However, it is indicated the total number of results, the 
    total number of pages and which page is currently shown. The user can move to the previous or to the subsequent page (if they exist) by clicking on the two 
    triangles near the page indicator, respectively, the leftward point triangle and the rightward point one. 
</p>
<p>
    If returned by the chosen catalog, search facets are shown on the right of the results list as a sequence of boxes.
    The heading of the box contains the name of the facet (e.g. <code>language</code>), while the contained items
    indicate different values for the specific facet (e.g. <code>English</code>, <code>French</code>, etc.). The number associated with
    each item indicates how many search results have that value for the facet. Users can refine their search by selecting
    one or more facets. To that end, it is sufficient to click on the item of interest. Active facets are rendered with a darker color,
    and they can be deactivated by clicking on them again.
</p>
<p>
    For each search result, the following information is shown:
</p>
<ul>
    <li><em>identifier</em> (e.g. <em>bio</em>)</li>
    <li><em>title</em> (e.g. <em>BIO: A vocabulary for biological information</em>)</li>
    <li><em>ontology IRI</em> (e.g. <em>http://purl.org/vocab/bio/0.1/</em>). Only available when the dataset can be imported as a single document through this
    ontology IRI</li>
    <li><em>description</em> (e.g. <em>A vocabulary for describing biographical information about people, both living and dead</em>)</li>
</ul>
<p>
    If a search result is associated with titles and descriptions in different natural languages, the display language is determined as follows
    in decreasing order of preference:
</p>
<ul>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/API/NavigatorLanguage/languages">user's preferred languages</a> in decreasing order of preference</li>
    <li>English</li>
    <li>any</li>
</ul>
<p>
    When the user clicks on a search result, its detailed description is shown on the right side of the dialog. Currently, the description includes a few additional metadata,
    most importantly the <em>data dump</em>, the <em>SPARQL endpoint</em> and the <em>URI prefix</em>. On the right of the dataset description, there are some boxes, 
    corresponding to catalog-specific facets (e.g. <em>language</em> and <em>tag</em>) used to classify the dataset.
</p>
<p>
    Catalogs may support different access methods (e.g. SPARQL endpoint or data dump), and the datasets in the same catalogs may support different methods.
</p>
<p>
    In general, the presence of specific metadata determines what can be done with a given dataset:
</p>
<ul>
    <li><em>preloading</em> requires a data dump</li>
    <li><em>adding an import</em> requires the ontology IRI, optionally benefiting from the data dump</li>
</ul>

</ui:define>
</body>
</html>