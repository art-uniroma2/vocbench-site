<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets"
  xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

<head>
  <title>
    <ui:insert name="title">Default title</ui:insert>
  </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link href="../../styles/art.css" rel="stylesheet" type="text/css" />
</head>

<body jsfc="ui:composition" template="/WEB-INF/templates/main-template-FILLERS.xhtml"
  xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets"
  xmlns:f="http://java.sun.com/jsf/core">

  <ui:define name="mainContent">

    <h1>Custom Services</h1>

    <div id="IndexDiv">
      <script type="text/javascript" src="../../js/indexCreator.js"></script>
    </div>

    <h2>Introduction</h2>

    <p>
      <em>Custom Services</em> provide a means to extend VocBench at the service-layer by enabling the definition of
      new services in a rather high-level manner. It is sufficient to have a general understanding of the conceptual
      model of the services (i.e. how they are composed), and the ability to express the desired behavior in some
      implementation language, currently, only SPARQL queries and updates. Custom services relieve the user from the
      need to know the underlying architecture of VocBench, the use of OSGi to support extendability, or the underlying
      programming model used to write services. Furthermore, there is no longer any need to set up a separate
      development environment, since all development is done within VocBench itself.
    </p>

    <h2>Conceptual Model</h2>
    <p>
      A <em>Custom Service</em> groups a number of related <em>operations</em> under a unique <em>service name</em>.
      Operations define the actual behavior of the service. Each operation is identified by a <em>name</em> (which shall
      be unique within a custom service), zero or more (formal) <em>parameters</em>, a <em>return type</em> and an
      <em>authorization</em> string. The actual behavior associated with the operation is defined by means of
      additional information specified by the chosen <em>CustomServiceBackend</em>: currently, there is only backend
      supporting the specification of an operation through a SPARQL query (for read services) or a SPARQL update (for
      mutation services).
    </p>
    <p>
      The operations defined by a custom service can be invoked by issuing a GET (for read services) or a POST (for
      mutation services) to addresses matching the following pattern:
    </p>
    <p>
      http://localhost:1979/semanticturkey/it.uniroma2.art.semanticturkey/st-custom-services/<em>service-name</em>/<em>operation-name</em>?ctx_project=<em>current-project</em>&amp;...parameter<sub>i</sub>=argument<sub>i</sub>
    </p>
    <h2>Editing Custom Services</h2>
    <p>
      The following figure depicts the main user interface for editing custom services.
    </p>
    <img src="images/custom-services.png" alt="Custom services tool" class="screenshot" />
    <p>
      The panel on the left lists the available <em>custom services</em>, providing buttons to <em>create</em> a new
      service,
      <em>delete</em> an existing one, and <em>refresh</em> the list. The dropdown menu associated with the refresh
      button offers the option to <em>force reload the services the file system</em>. This option is only useful if
      custom services are edited outside of VocBench, by editing the underlying representation on the file system. The
      items of the list are the <em>service identifiers</em> of the custom services. Under the hood, these identifiers
      identify configurations used to persist the definition of custom services in the configuration manager
      <em>it.uniroma2.art.semanticturkey.config.customservice.CustomServiceDefinitionStore</em>.
    </p>
    <p>
      The panel on the right shows the specification of any custom service that has been selected in the list on the
      left. The specification consists of a <em>service name</em>, <em>description</em> and a list of
      <em>operations</em>. The button labeled with a pencil near the first two fields can be used to edit the
      corresponding values. The list of operations is associated with buttons to <em>create</em> a new operation and
      <em>delete</em> an existing operation. The combined <em>refresh</em> button in the header of the right panel
      behaves like the already mentioned button, but limited to the currently selected service.
    </p>
    <p>
      After selecting an operation, the user interface is extended with a further panel showing its details, as depicted
      by the figure below.
    </p>
    <img src="images/custom-services-operation-details.png" alt="Detailed view of a custom service operation"
      class="screenshot" />

    <h3>Defining the operation authorization</h3>
    <p>
      The <em>authorization</em> string associated with an operation is used to determine whether an invocation of the
      operation should be allowed or forbidden (based on the user doing the invocation).
    </p>
    <p>The authorization string consists of three parts:</p>
    <ul>
      <li>a <em><a href="roles_adm.jsf#editing_capabilities">capability</a></em> that must be owned by the user who
        invokes the operation. The <em>subject</em> of the capability may be restricted to a given role (e.g. <em>concept</em>)
        or to the role of one of actual parameters of the operation</li>
      <li>optionally, a language that must be assigned to the user who invokes the operation. The language can be a
        constant (e.g. <em>en</em>) or be determined at runtime from one of the actual parameters of the operation</li>
      <li>one or more operations among the ones defined by our extended <em>CRUDV</em> model</li>
    </ul>
    <p>
      If no authorization is provided, the following defaults apply:
    </p>
    <ul>
      <li><em>rdf, R</em> for read operations</li>
      <li><em>rdf, CUD</em> for mutation operations</li>
    </ul>
    <p>
      The screenshot below depicts the wizard for the definition of an authorization string.
    </p>
    <p>
      <img src="images/custom-services-operation-authorization.png" alt="Detailed view of a custom service operation"
        class="screenshot" />
    </p>
    <h3>Defining Custom Services Operations in SPARQL</h3>
    <p>
      The parameters are mapped to variables with the same name in the SPARQL query. Actual parameters passed to a
      service upon invocation are pre-bound to the corresponding variable in the SPARQL query. A reserved variable is
      <em>workingGraph</em>, which is bound by default to the working graph of the project.
    </p>
    <p>
      The allowed parameter types are:
    </p>
    <ul>
      <li>boolean</li>
      <li>integer</li>
      <li>short</li>
      <li>long</li>
      <li>float</li>
      <li>double</li>
      <li>java.lang.String</li>
      <li>BNode</li>
      <li>IRI</li>
      <li>Literal</li>
      <li>Resource</li>
      <li>RDFValue</li>
    </ul>
    <p>
      The allowed return type depends on the actual SPARQL operation:
    </p>
    <ul>
      <li>Update =&gt; void</li>
      <li>ASK =&gt; boolean</li>
      <li>SELECT =&gt;
        <ul>
          <li>List&lt;T&gt;, where T
            <ul>
              <li>might be any of the above accepted parameter types (in this case the SELECT must return one variable)
              </li>
              <li>be AnnotatedValue&lt;S&gt; where S might be:
                <ul>
                  <li>BNode</li>
                  <li>IRI</li>
                  <li>Resource
                    (in this case the SELECT must return one variable plus additional variables ?attr_... for additional
                    attributes)</li>
                </ul>
              </li>
            </ul>
          </li>
          <li>any of the above accepted parameter types (in this case the SELECT must return one variable and produce
            one result)</li>
          <li>TupleQueryResult (corresponding to the raw result set produced by the query)</li>
        </ul>
      </li>
    </ul>
  </ui:define>
</body>

</html>