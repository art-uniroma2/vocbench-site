<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

<head>
    <title>
        <ui:insert name="title">Roles administration</ui:insert>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../../styles/art.css" rel="stylesheet" type="text/css" />
</head>

<body jsfc="ui:composition" template="/WEB-INF/templates/main-template-FILLERS.xhtml" xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:f="http://java.sun.com/jsf/core">

    <ui:define name="mainContent">

        <h1>Roles administration</h1>

        <div id="IndexDiv">
            <script type="text/javascript" src="../../js/indexCreator.js"></script>
        </div>

        <h2>Introduction</h2>

        <p>The Role Management page lists all roles defined in the system and allows authorized users to edit them. Roles define sets of capabilities that can be assigned to existing users, per project. The &quot;per project&quot; means that a given user could be assigned the role of &quot;Project Manager&quot; in a particular project, while being only a &quot;Lexicographer&quot; in another project.</p>
        <p>The assignment of roles to users is not covered in this page, it is only possible from the <a href="projects_adm.jsf#project-users_management">Project-Users management</a> panel in the Project management page.</p>
        
        <h2>The Roles panel</h2>
        <p>The left panel lists all the available roles. If a role is grayed out, it means that it has been defined at the factory level and is therefore not available for editing. The right panel lists the capabilities for the selected role. Roles can only be created when the user is logged in to a specific project, as newly created roles are stored locally for each project.</p>
        <p>The buttons on the left panel allow respectively to: create a new role, delete a role (not possible for factory-defined roles), import a role from a saved description, or export a role.</p>
        <p>User-defined roles can be modified by defining/editing their capabilities through the right panel.</p>
        <div class="figure"><img src="images/rolemanagement.png" alt="Role Management" /></div>
        
        <p>Below is a description of predefined roles in the system and their permissions. </p>
        <ul>
          <li><em>Project Managers</em> have administrator rights local to the projects for which they have been granted this role.</li>
          <li><em>Ontology editors</em> are allowed to perform basic editing actions and more complex ontology modeling, such as expressing logical axioms.</li>
          <li><em>Thesaurus editors</em> are allowed to perform basic editing actions and to maintain thesauri by means of the SKOS vocabulary</li>
          <li><em>Lexicographers</em> (and terminologists) are allowed to edit lexicalizations (for any type of lexical model: rdfs:labels, skos/skos-xl terminological labels, ontolex entries). This role can be limited to certain languages.</li>
          <li><em>Mappers</em> are allowed to perform alignments only.</li>
          <li><em>Validators</em> are allowed to validate the actions of other users (useful when <em>Validation</em> is active in the given project)</li>
          <li><em>RDF geeks</em> are allowed to perform any operation on RDF data, edit and run Reports and Custom Services.</li>
          <li><em>Lurkers</em> can read everything in a project, but are not allowed to edit anything.</li>
        </ul>
        <p>Note that <em>Administrator</em> is not a role, rather a type of user, this is why it does not appear in the roles panel. The administrator has full access to the system and no restriction on any functionality, this is why it is worthless to assign any role to an administrator. Any instance of VocBench is initialized with one administrator, who can later one nominate other administrators. Another type of user is the <em>super user</em>. Super users have one single superpower: they can create projects and automatically be endowed with the <em>project manager</em> role for their created projects. Besides this, they are normal users, so they can be given roles different from project manager in other projects, or they can be even removed the role of project manager from the administrator in a project they created.</p><p>To know exactly what each role can do, the role capabilities must be examined. For example, by looking at the figure above and the information given in the following sections, you can see exactly what permissions a lexicographer has, apart from editing labels and notes.</p>  
 
        <h2>Editing Capabilities</h2>
        <p>We have created a dedicated <em>capability language</em> for expressing user capabilities. The capability language is based on the <a href="https://it.wikipedia.org/wiki/Prolog">Prolog</a> logic programming language and allows for a rich set of expressions, supported by entailments (e.g. specific capabilities can be entailed by more general ones, based on a concept of &quot;coverage&quot; expressed in the capability). </p>
        <p>In order for a user to perform an action, the range of capabilities owned by the user must satisfy those required by the action. The Prolog-based technology for checking capabilities is available both server and client side. The engine in the server provides the ultimate check for authorization (as requests could be performed by bypassing the client) while the client one is used to enable visualization/activation of UI elements, depending on the logged user. This way, it is possible to have very dynamic UIs automatically modeled on users' capabilities, which are ultimately defined in the service code, with no further redundancy in the code.</p>
        <p>As <a href="http://semanticturkey.uniroma2.it">Semantic Turkey</a> is based on Java, server-side we adopted <a href="http://apice.unibo.it/xwiki/bin/view/Tuprolog/">tuProlog</a>, a Java-based light-weight Prolog system for distributed applications and infrastructures, developed by the <a href="http://apice.unibo.it/">APICe</a> research labs of the <a href="http://www.unibo.it/">University of Bologna</a></p>
        <p>For the client, our choice fell on <a href="https://github.com/Sleepyowl/jsprolog/">jsprolog</a>, a simple Prolog interpreter written with TypeScript. Thanks to <a href="https://github.com/Sleepyowl">Dmitry Soloviev</a> for his constant help and improvements he brought to jsprolog so that we could adopt it in VocBench.</p>
        <p>In general, a capability is expressed by:</p>
        <ul>
            <li>an <em>Area</em> (e.g. <em>RDF</em>, as of all operations on RDF data, or <em>RBAC</em>, as the set of management operations related to Role Based Access Control)</li>
            <li>a <em>Subject</em> (the element on which an operation is being performed, e.g. <em>user</em>s)</li>
            <li>a <em>Scope</em> (a scoping modifier for the subject)</li>
            <li>the set of allowed operations on the above, expressed through the traditional <em>CRUD</em> pattern (<em>Create</em>, <em>Read</em>, <em>Update</em>, <em>Delete</em>), extended with a <em>Validate</em> operation.</li>
        </ul>
        <p>The vocabulary for Subject and Scope varies depending on the chosen Area (see next section on vocabulary)</p>
        <p>Capabilities are expressed by formulas in the following form:</p>
        <p class="code">capability(&lt;area&gt;(&lt;subject&gt;,&lt;scope&gt;),&lt;OperationSet&gt;)</p>
        <p>The <em>OperationSet</em> is represented by any combination of the letters C,R,U,D,V, in between ' ' (e.g. 'CU', 'R', 'CRUD', 'V')</p>
        <p>In some cases, monadic variations of the area specification are admitted, in the form:</p>
        <p class="code">capability(&lt;area&gt;(&lt;term&gt;),&lt;OperationSet&gt;).</p>
        <p> where term can be the scope, or another special term. There is no general semantics for the monadic variations, and these are explained case by case. </p>
        <p>Specifications of the sole area are also possible, usually implying unlimited power over that area, as in this example:</p>
        <p class="code">capability(rdf,'CRUDV').</p>
        <p>representing the capability to perform any operation on the RDF data.</p>
        <p>The capability editor (in the following figure) offers a wizard for completing most of the capability description without writing any code.</p>
        <div class="figure"><img src="images/capabilityediting.png" alt="Editing Capabilities" /></div>
        <p>The grey text area at the bottom of the editor shows the composed capability expression.</p>

        <h2>Capabilities Vocabulary</h2>
        <p>The following table describes the range of possible combos of <em>Subjects</em> and <em>Scopes</em> for each Area. Monadic expressions provide short syntactic variations for various combinations of subject and scopes, or further semantics.</p>
        <table width="1428" border="1">
            <tbody>
                <tr>
                    <th bgcolor="#305A5F" scope="col" width="200"><strong style="color: #FFFFFF">Area</strong></th>
                    <th bgcolor="#305A5F" scope="col" width="400"><strong style="color: #FFFFFF">Subject</strong></th>
                    <th bgcolor="#305A5F" scope="col" width="400"><strong style="color: #FFFFFF">Scopes</strong></th>
                    <th bgcolor="#305A5F" scope="col" width="400"><strong style="color: #FFFFFF">Monadic Variations of the Scopes</strong></th>
                </tr>
                <tr>
                    <th height="331" scope="row">
                        <p>RDF (rdf)</p>
                        <p><em>Note: </em></p>
                        <p><em>rdf</em></p>
                        <p><em> with no brackets implies all operations on the RDF area.</em></p>
                    </th>
                    <td valign="top">
                        <p>one of:</p>
                        <p><em>resource<br />
                                cls<br />
                                individual<br />
                                property<br />
                                objectProperty<br />
                                datatypeProperty<br />
                                annotationProperty<br />
                                ontologyProperty<br />
                                ontology<br />
                                dataRange<br />
                                datatype<br />
                                concept<br />
                                conceptScheme<br />
                                xLabel<br />
                                xLabel(&lt;language&gt;)<br />
                                skosCollection<br />
                                skosOrderedCollection<br />
                                ontolexForm<br />
                                ontolexLexicalEntry<br />
                                limeLexicon<br />
				graph
                            </em></p>
                        <p>where &lt;language&gt; must be expressed between &quot;&quot; (e.g. &quot;en&quot;). xLabel without any language allows operations on xLabels for any language</p>
                        <p>The following entailments hold between capabilities made explicit on a subject (on the left) and other subjects covered by the expressed capability</p>
                        <ul>
                            <li>resource --&gt; any resource</li>
                            <li>property --&gt; objectProperty</li>
                            <li>property --&gt; datatypeProperty</li>
                            <li>property --&gt; annotationProperty</li>
                            <li>property --&gt; ontologyProperty</li>
                            <li>skosCollection --&gt; skosOrderedCollection</li>
                        </ul>
                        <p>graphs (through the subject <em>graph</em>) are managed both as resources and as their content, in particular, for the sole rdf(graph), the crud is interpreted as follows:
  			  <ul>
                            <li>C: creates a new named graph</li>
                            <li>R: reads the list of available named graphs</li>
                            <li>U: updates a graph (any other than the working graph, which can be written by default), intended as the content (the triples in the graph); notice that this must be used in combination with another capability for the specific operation</li>
                            <li>D: drops a named graph		</li>
			  </ul>
			  metadata about the resource identifying the graph can instead be modified according the subjects used for all the other resources (e.g. rdf(graph, values) for values of triples having the graph as subject)	    			    			    
			</p>
                    </td>
                    <td valign="top">
                        <p>one of:</p>
                        <ul>
                            <li><em>
                                    values (applicable to all subjects) </em><br />
                                <br />
                            </li>
                            <li><em>alignment (applicable to all subjects) <br />
                                </em>allows the authorized user to perform mapping operations on the specified subject<br />
                                <br />
                            </li>
                            <li><em>instances (applicable to subject: &quot;cls&quot; only)</em><em> <br />
                                </em>allows the authenticated user to perform operations related to the instances of the subject <em>class</em><br />
                                <br />
                            </li>
                            <li><em>lexicalization (applicable to all subjects) <br />
                                </em>allows the authorized user to perform operations on any kind of lexicalization for the given subject<br />
                                <br />
                            </li>
                            <li><em>notes (applicable to all subjects)</em><br />
                                allows the authorized user to perform operations on any kind of note for the given subject<br />
                                <br />
                            </li>
                            <li><em>domain</em> <em> (applicable to all property type of subjects) </em><br />
                                allows for performing operations on the domain of the subject property<br />
                                <br />
                            </li>
                            <li><em>range <em> (applicable to all property type of subjects) </em><br />
                                </em>allows for performing operations on the range of the subject property<br />
                                <br />
                            </li>
                            <li><em>schemes <em> (applicable to concept, conceptScheme, skosCollection, skosOrderedCollection, xLabel) </em></em><br />
                                allows the authorized user to perform operations on the relationship between the subject and existing schemes<em> <br />
                                    <br />
                                </em></li>
                            <li><em>taxonomy (applicable to concepts, classes, properties, collections</em>)<br />
                                covers all kind of taxonomic relationship, when applicable to the subject  (e.g. for concepts, classes, properties, collections, intended as the containment relation between collections)</li>
                            <br />
                            <li><em>formRepresentations (applicable to ontolexForm)</em></li>
                            <br />
                            <li><em>subterms (applicable to ontolexLexicalEntry)</em></li>
                            <br />
                            <li><em>constituents (applicable to ontolexLexicalEntry)</em></li>
                            <br />
                        </ul>
                    </td>
                    <td valign="top">
                        <ul>
                            <li><em>any of the available subject resources</em>. A monadic expression containing only a resource as a subject allows to perform all</li>
                            <li><em>lexicalization</em>: this means that the user is able to perform any operation related to lexicalizations for any resource</li>
                            <li><em>import</em>: any operation related to data import</li>
                            <li><em>sparql</em>: any operation via SPARQL. Concerning the <em>CRUD</em>, only <em>R</em> is required for queries and <em>U</em> for updates.</li>
                            <li><em>skos</em>: any operation specific to the skos model</li>
                            <li><em>ontolex</em>: any operation specific to the ontolex model</li>
                        </ul>
                        <p>&nbsp;</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <p>Role Based Access Control (rbac)</p>
                        <p>&nbsp;</p>
                    </th>
                    <td valign="top">
                        <p><em>role</em></p>
                        <p><em>user</em></p>
                    </td>
                    <td valign="top">
                        <ul>
                            <li><em>capability</em>: appliable to <em>role</em> only, allows the authenticated user to perform operations related to capabilities for the subject role</li>
                            <li><em>role</em>: appliable to user only, allows the authenticated user to perform operations related to roles for the subject user</li>
                        </ul>
                    </td>
                    <td valign="top"><em>role</em>: this capability is needed for operations centered on roles, with no relations to users/capabilities</td>
                </tr>
                <tr>
                    <th scope="row">Project Management (pm)</th>
                    <td valign="top">
                        <p><em>project</em><br />
                        </p>
                    </td>
                    <td valign="top">
                        <p>Any of the following scopes can be associated to the subject <em>project</em></p>
                        <ul>
                            <li><em>baseuri</em></li>
                            <li><em>defnamespace</em></li>
                            <li><em>prefixMapping</em></li>
                            <li>group</li>
                            <li>collaboration</li>
                        </ul>
                    </td>
                    <td valign="top"> <em>project</em>: for operations centered on a project (read, create etc..), with no relationship to other aspects</td>
                </tr>
                <tr>
                    <th scope="row">User Management (um)</th>
                    <td valign="top"> <em>user</em><br /></td>
                    <td valign="top">
                        <p>Any of the following scopes can be associated to the subject <em>user</em></p>
                        <ul>
                            <li><em>activation</em>: allows the authorized user to activate the subject <em>user</em></li>
                            <li><em>project: </em>allows the authorized user to associate the subject user to a given project</li>
                        </ul>
                    </td>
                    <td valign="top"><em>user</em>: for operations centered on users (read, create etc..), with no relationship to other aspects</td>
                </tr>
                <tr>
                    <th scope="row">Custom Forms (cform)</th>
                    <td valign="top">
                        <ul>
                            <li><em>form</em>: </li>
                            <li> <em>formCollection</em>: a collection of forms</li>
                        </ul>
                    </td>
                    <td valign="top">
                        <p>The following scopes are available:</p>
                        <ul>
                            <li><em>mapping</em>: associable to the <em>form</em> subject, it allows the authorized user to perform operations related to form mapping</li>
                            <li><em>form</em>: associable to the <em>formCollection</em> subject, it allows the authorized user to perform operations related to the association between forms and formCollections</li>
                        </ul>
                    </td>
                    <td valign="top"><em>form</em>: for operations centered on forms (read, create etc..), with no relationship to other aspects</td>
                </tr>
                <tr>
                    <th scope="row">System (sys)</th>
                    <td valign="top">
                        <p>The following subjects are available:</p>
                        <ul>
                            <li>metadataRegistry</li>
                            <li>ontologyMirror</li>
                            <li>plugins</li>
                        </ul>
                    </td>
                    <td valign="top">No scopes are available for the moment, all system expressions are (for now) monadic</td>
                    <td valign="top">&nbsp;</td>
                </tr>
            </tbody>
        </table>


    </ui:define>
</body>

</html>