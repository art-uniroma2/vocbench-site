<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

<head>
	<title>
		<ui:insert name="title">Default title</ui:insert>
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="../../styles/art.css" rel="stylesheet" type="text/css" />
</head>

<body jsfc="ui:composition" template="/WEB-INF/templates/main-template-FILLERS.xhtml"
	xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets"
	xmlns:f="http://java.sun.com/jsf/core">

	<ui:define name="mainContent">

		<h1>Alignment Systems</h1>

		<div id="IndexDiv">
			<script type="text/javascript" src="../../js/indexCreator.js"></script>
		</div>

		<h2>Introduction</h2>

		<p>
			VocBench 3 can use remote services to compute alignment between two datasets (associated with distinct
			projects), as described in the page on the <a href="alignment_validation.jsf">alignment validation tool</a>.
		</p>

		<h2>Alignment services API</h2>
		<p>
			The communication between VocBench 3 and a remote alignment service is carried on using a REST API, which
			supports activities including task management and settings retrieval. This API has been formally described
			using the <a href="https://openapis.org/">OpenAPI Specification</a> format. Such an API description can be
			used in a number of <a href="https://openapi.tools/">tools</a> to perform several API-related activities,
			such as generation of server stubs, generation of client libraries, testing, documentation, etc. In
			particular, code generators (such as <a href="https://swagger.io/tools/swagger-codegen/">Swagger Codegen</a>,
			with its <a href="https://editor.swagger.io/">online editor</a> 
			and <a href="https://openapi-generator.tech/">OpenAPI Generator</a>) can significantly help to develop a
			wrapper for an existing matching system, supporting dozens of programming languages and frameworks, while
			also fostering compliance with the API specification.
		</p>
		<p>
			The description of the current version of the API is available online at this address <a
				href="http://art.uniroma2.it/maple/specs/alignment-services-2.0.0.yaml">http://art.uniroma2.it/maple/specs/alignment-services-2.0.0.yaml</a>
		</p>
		<p>
			See the section below for a <a href="#reference_of_the_alignment_services_api">reference</a> of the API.
		</p>

		<h2>Alignment process</h2>
		<p>
			We assume that the user chose two align two datasets stored as projects in VocBench 3. Metadata about these
			datasets and other, potentially available resources, should be available in the <a
				href="http://localhost:8080/vocbench-site/doc/user/mdr.jsf">metadata registry</a>. The following
			description will be grounded in the <a href="alignment_validation.jsf">alignment validation tool</a>: in
			particular, the user is assumed to be at the point of creating a <a
				href="alignment_validation.jsf#alignment_from_remote_alignment_system">new task on a remote system</a>.
		</p>

		<div class="figure"><img src="images/alignmentvalidation-newtask.png" alt="alignment remote new task" /></div>

		<ul>
			<li>When the user clicks the "Profile" button, VocBench 3 uses <a
					href="http://art.uniroma2.it/maple">MAPLE</a> to analyze the matching problem,
				obtaining a report describing the <em>alignment scenario</em>.

				<div class="figure"><img src="images/alignmentvalidation-alignment-scenario.png"
						alt="alignment scenario" /></div>

				<p>
					At the top of the scenario, we can find metadata about the input datasets, including their namespace
					(called
					<em>URI space</em>), knowledge model (labelled <em>conformsTo</em>) and SPARQL endpoint.
				</p>
				<p>
					MAPLE is unaware about the actual algorithm used to match the input datasets, but it assumed that
					their
					lexical content plays a pivotal role. In the terminology of MAPLE, based on the metadata model <a
						href="http://art.uniroma2.it/lime">LIME</a>, a set of labels of a dataset is called a
					<em>lexicalization
						set</em>. Based on the previously stated assumption, an important part of the scenario
					description
					consists of a list of pairs of lexicalization sets for the input datasets: the <em>pairings</em>
					section in
					the figure. Each pairing is intended to suggest strategy to compare the input dataset at the lexical
					level,
					possibly benefiting from a wordnet-like resource.
				</p>
			</li>
			<li>
				the user can establish a concrete <em>scenario definition</em>, by choosing among the alternatives
			proposed in the scenario. Examples of entries found in different scenarios are:
				<ul>
				  <li>lexicalization pairings (i.e. couple of lexicalizations, one from each of the datasets to be aligned)</li>
				  <li>language resource (if any) to use as a support to the alignment (e.g. a WordNet in Italian could be used as a lexical bridge for augmenting the potential lexical overlap between two lexicalizations in Italian</li>
				  <li>alignment chains: instead of relying on language, it is possible to use existing alignments to a third resource as a bridge</li>
	          </ul>
			</li>
			<li>The choice of the scenario
				        is an optional step: as an alternative, the system will create a scenario definition by choosing just the
				        first pairing (the one with the highest score), discarding any language resource. </li>
			<li>
				once a <em>scenario definition</em> has been established, the user can click the "search" link in the
				"matchers" panel in the bottom, to retrieve matchers configurations suitable for this scenario
				definition.

				<div class="figure"><img src="images/alignmentvalidation-candidate-matchers.png"
						alt="candidate matchers for the scenario" /></div>

				this step is optional, as the alignment service is expected to have a default configuration that is
				suitable for most situations. However, at user's willingness, this step allows to act on the
				configuration knobs exposed by alignment service. As they depend on the actual scenario definition,
				these matchers configuration shouldn't offer meaningless options: e.g. different options related to
				synonym expansion if no language resource has been selected (and the service doesn't embed any).
			</li>
			<li>
				the user clicks on the button "OK" to create a new alignment task, based on the <em>alignment plan</em>,
				consisting of:
				<ul>
					<li>scenario definition</li>
					<li>optional settings</li>
				</ul>
				This task will be executed asynchronously, on the remote alignment service, since alignment tasks may require considerable time to complete.
			</li>
		</ul>
		<h2>Compliant services</h2>
		<p>
			Hereafter, we report on alignment systems that have been made compatible with VocBench 3 by implementing our alignment API.
		</p>

		<h3>Ge.no.ma</h3>
		<p>
			<a href="https://bitbucket.org/art-uniroma2/genoma">Ge.no.ma</a> is an Ontology Matching Environment that
			provides the user with a powerful tool to design and test ontology matching architectures.
		</p>
		<p>
			Genoma is available, on its <a href="https://bitbucket.org/art-uniroma2/genoma/downloads/">downloads</a> page, as an
			archive called <em>genoma-alignment-service-#{genoma_version}.zip</em>. To execute Genoma as a VocBench 3 service,
			it is sufficient to unpack the archive and execute either <em>start.bat</em> or <em>start.sh</em>,
			depending on whether the host operating system is Windows or a UNIX system.
		</p>
		<p>
			Once launched, Genoma will be listening on address <a
				href="http://localhost:7575">http://localhost:7575</a>. Opening this address in the browser should load
			an interactive documentation (based on <a href="https://swagger.io/tools/swagger-ui/">Swagger UI</a>), as in the
			following picture.
		</p>

		<div class="figure"><img src="images/genoma-endpoint-web.png" alt="Genoma interactive documentation" /></div>

		<h3>NAISC</h3>
		<p>
			<a href="https://bitbucket.org/art-uniroma2/genoma">NAISC</a> is an automated linking tool developed at the
			<a href="https://www.insight-centre.org/">Insight Centre for Data Analytics</a>. 'Naisc' means 'links' in
			Irish and is pronounced 'nashk'.
		</p>
		<p>
			NAISC has its own REST API, which is being extended with further endpoints complying with our API
			specification. This support is currently available on the <a
				href="https://github.com/insight-centre/naisc/tree/dev">dev</a> branch.
		</p>
		<p>
			To launch NAISC as a VocBench 3 service, it is necessary to:
		</p>
		<ul>
			<li>download the <em>dev</em> branch</li>
			<li>on Windows, inside the folder <em>naisc-rest</em>:
				<ul>
					<li>replace the file <em>models</em> (which is intended to be a symbolic link) with an empty
						directory</li>
					<li>replace the file <em>configs</em> (which is intended to be a symbolic link) with a copy of the
						homonym directory in the root of the projects sources</li>
				</ul>
			</li>
			<li>
				execute either <em>gradlew.bat</em> (on Windows) or <em>gradlew</em> (on a unix-like system)
			</li>
			<li>armed with patience, wait until Naisc has downloaded its models</li>
		</ul>

		<p>
			When running, Naisc listens for request at this address <a
				href="http://localhost:8080/naisc-rest/maple">http://localhost:8080/naisc-rest/maple</a>. Opening this
			address in the browser should return metadata about the service as a JSON object.
		</p>


		<h3>Alignment Bootstrap</h3>
		<p> The Alignment Bootstrap is a set of services currently hosted on the ST Remote Service Compendium (<a href="https://bitbucket.org/art-uniroma2/st-rsc/">ST-RSC</a>), which exploits alignment chains (see previous section) to generate alignments between datasets bridged by alignments to a third one.</p>
		
		<p>Once the ST-RSC service has been activated, the Alignment Bootstrap services will be listening on address
	      <a href="http://localhost:7576/st-rsc/align">http://localhost:7576/st-rsc/align</a> (the standard url/port can be changed by modifying the startup script), so this is the URL to be used when connecting VB to the ST-RSC server.</p>
		<div class="figure"><img src="images/st-rsc-endpoint-configuration.png" alt="ST-RSC configuration in VB" /></div>
		<p>
		ST-RSC's services are described in a <a href="https://bitbucket.org/art-uniroma2/st-rsc/src/master/st-rsc.yaml">YAML file</a>, 
		the model of which is based on OpenAPI 3.0.1. Note that the ST Remote Service Compendium offers other services, such as the <a href="skos_diffing.jsf">Skos Diffing</a>.</p>
		<div class="figure"><img src="images/st-rsc-endpoint-web.png" alt="ST-RSC interactive documentation" /></div>

		<h2>Reference of the Alignment services API</h2>
		<div id="swagger-ui"></div>
		<script src="https://unpkg.com/swagger-ui-dist@3.12.1/swagger-ui-bundle.js"></script>
		<script src="https://unpkg.com/swagger-ui-dist@3.12.1/swagger-ui-standalone-preset.js"></script>
		<script type="text/javascript">
			(function () {
				var head = document.getElementsByTagName('head')[0];
				var cssnode = document.createElement('link');

				cssnode.type = 'text/css';
				cssnode.rel = 'stylesheet';
				cssnode.href = "https://unpkg.com/swagger-ui-dist@3.12.1/swagger-ui.css";

				head.appendChild(cssnode);
			})();

			window.addEventListener("load", function () {
				// Build a system
				const ui = SwaggerUIBundle({
					url: "https://bitbucket.org/art-uniroma2/maple/raw/master/maple-alignment-services-api/src/main/openapi/alignment-services.yaml",
					dom_id: '#swagger-ui',
					deepLinking: true,
					presets: [
						SwaggerUIBundle.presets.apis,
						SwaggerUIStandalonePreset
					],
					plugins: [
						SwaggerUIBundle.plugins.DownloadUrl
					],
					layout: "StandaloneLayout",
				})
				window.ui = ui
			});
		</script>
	</ui:define>
</body>

</html>