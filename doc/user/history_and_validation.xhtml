<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

<head>
  <title>
    <ui:insert name="title">Default title</ui:insert>
  </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link href="../../styles/art.css" rel="stylesheet" type="text/css" />
</head>

<body jsfc="ui:composition" template="/WEB-INF/templates/main-template-FILLERS.xhtml" xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:f="http://java.sun.com/jsf/core">

  <ui:define name="mainContent">

    <h1>History and Validation</h1>
    <div id="IndexDiv">
      <script type="text/javascript" src="../../js/indexCreator.js"></script>
    </div>

    <h2>How are History and Validation Managed</h2>
    <p>VB3 implements a track-change mechanism working at triple-level. Triples removed/added by each action are reified, grouped around a common resource representing the action that produced the change and stored in a separated (but connected to the project) RDF repository (the <em>support repository</em>) together with the actions&rsquo; metadata about the invoked action and the context of its invocation. </p>
    <p>The change-tracking mechanism has been implemented as a <em>sail</em> for the RDF4J framework (<a href="http://rdf4j.org/">http://rdf4j.org/</a>). The sail is embedded within the system, but can also be deployed as a pluggable component inside other sail-compliant triple stores (see the section of the system administration manual on using a <a href="../sys/#separate_triple_store">Separate Triple Store</a>).</p>

    <p>
      During the project creation it is possible to enable the <i>History</i>, <i>Validation</i> and <i>Blacklisting</i> features. The latter can be enabled only if the Validation is enabled as well.
    </p>
    <div class="figure">
      <img src="images/history_validation_blacklist.png" alt="Create project HVB" />
    </div>
    
    <h2>Validation</h2>
    <p>In projects requiring <em>validation</em> of the performed actions, changes are not directly affecting the core data. In fact, changes are stored in the support repository (as described in the introduction, in the form of reified triples with action metadata) and in the form of plain (non reified) triples in a dedicated graph (the <em>staging graph</em>) of the core repository.</p>
    <p>The staging graph is used to allow users to be immediately notified of the changes brought to the repository, even though they have still not been confirmed. In particular, changes will be represented in the various views (e.g. the values represented in the resource-view and the various resource trees on the left) available in VocBench according to the following conventions: 
      <ul>
        <li>in <span style="color: green; font-style: italic">green, italic</span>: added triples (e.g. relationships shown in the resource view)</li>
        <li><span style="background-color: #dff0d8; border: 1px solid #d6e9c6; border-radius: 4px">backgrounded in green</span>: added resources as wholes (i.e. as RDF as no &quot;containing frame&quot; for resources and their description is sparse across the various triples mentioning them, the existence of a resource is identified as the presence of one or more type declarations, thus introduced with the predicate<span class="code"> rdf:type</span>)</li>
        <li>in <span style="color: darkred; text-decoration: line-through">red, stroke-through</span>: removed triples (e.g. relationships shown in the resource view)</li>
        <li><span style="background-color: #f2dede; border: 1px solid #ebccd1; border-radius: 4px">backgrounded in red</span>: removed resources as wholes </li>
      </ul>
       as in figure below.<br/>
    </p>

    <div class="figure">
      <img src="images/validation_resource-view.png" alt="Validation View" />
    </div>
    <p>
      It is possible for authorized users to access the list of actions to be validated through the Validation View (&quot;Validation&quot; button from the menu in the project stripe). In the figure below, it is possible to see, for each action, the identifier of the committed transaction, the type of action (column &quot;Action&quot;), the list of parameters of the action, the user who performed the action and the date and time when it was performed. The combo-box at the end of the row allows the user in charge of validating the actions to accept or reject each action.
    </p>
    <p>
      Users with no validation capabilities can still access the <i>Validation</i> page; however the view is limited to their actions only and they can only reject them (<i>Accept</i> not allowed).
    </p>

    <div class="figure">
      <img src="images/validation_view.png" alt="Validation View" />
    </div>

    <p>The number of shown parameters is dynamic in the size of the window, though it is possible to inspect all of them by clicking on the [...] symbol that appears when at least one parameter has been hidden due to lack of space, as shown in the figure below:</p>
    <div class="figure">
      <img src="images/validation_parameters.png" alt="Validation Parameters" />
    </div>


    <p>If the metadata of the performed action is not enough to evaluate the change, by clicking on the commit identifier it is also possible to inspect the triples that have been added/removed by the change. In figure below, the four triples added by an &quot;addAltLabel&quot; action are being shown.</p>

    <div class="figure">
      <img src="images/validation_triples-inspection.png" alt="Validation Parameters" />
    </div>


    <h2>History</h2>
    <p>If history is enabled in the project, it is possible to access the list of performed actions through the &quot;History&quot; button from the menu in the project stripe. In the figure below, the History View is shown. It is identical to the Validation View, except for the absence of the &quot;Validate&quot; column</p>
    <div class="figure">
      <img src="images/history_view.png" alt="History View" />
    </div>

    <h2>Commits Filter</h2>

    <p>
      The filter panel in the History and Validation pages allows users to refine their search criteria to find specific commits or staging commits within the application. The following filters are available:
    </p>
    <ul>
      <li>
        <i>Operations</i>: this filter allows users to narrow down their search based on the type of operation performed. These operations correspond directly to the underlying services invoked when users interact with the system.
      </li>
      <li>
        <i>Performers</i>: by selecting one or more users from the list, users can filter commits based on the individuals who executed the actions.
      </li>
      <li>
        <i>Time</i>: defines a specific range within which the operations were performed.
      </li>
      <li>
        <i>Resource</i>: by entering the IRI of a resource, users can filter commits to only display those related to the specified resource.
      </li>
    </ul>

    <div class="figure"><img src="images/history_validation_filters.png" /></div>

    <p>
      The <i>Resource</i> filter is particularly useful when tracking changes to specific entities of the dataset. 
      Within the ResourceView, users will find links specifically tailored to redirect to the History and Validation pages. These links are designed to automatically pre-fill the <i>Resource</i> filter with the IRI of the resource currently being viewed.
    </p>

    <div class="figure"><img src="images/history_validation_resview_links.png" /></div>

    <h2>Blacklisting</h2>
    <p>
      If <i>blacklisting</i> is enabled, some operations that involve literal content, might be blacklisted, namely if the operation is rejected, further actions that contain the same literal, will be automatically blocked. For example, if the addition of the label <span class="code">"contrattisti"@en</span> is rejected, future addition of this label to the a resource, or the creation of a new resource with this label, will be rejected. Anyway the addition could be forced.
    </p>
    <div class="figure">
      <img src="images/blacklisted_addition.png"/>
    </div>

    <p>
      In the validation view, for those operations allowing the blacklisting, a comment button is available and it is enabled only when the operation is rejected. Through this button is possible (optionally, but highly recommended) to provide a comment for motivating the reason of the rejection. This comment will be shown in the error message (shown in the previous image) each time the user will try to add blacklisted values.
    </p>
    <div class="figure">
      <img src="images/validation_blacklist.png"/>
    </div>
    
  </ui:define>
</body>

</html>