<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html">

<head>
	<title>
		<ui:insert name="title">Default title</ui:insert>
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="../../styles/art.css" rel="stylesheet" type="text/css" />
</head>

<body jsfc="ui:composition" template="/WEB-INF/templates/main-template-FILLERS.xhtml" xmlns="http://www.w3.org/1999/xhtml" xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:f="http://java.sun.com/jsf/core">

	<ui:define name="mainContent">

		<h1>Alignment Validation</h1>

		<div id="IndexDiv">
			<script type="text/javascript" src="../../js/indexCreator.js"></script>
		</div>

		<h2>Introduction</h2>

		<p>
			The Alignment Validation tool can be accessed from the VB main toolbar. This tool allows the management of alignment from two different source:
		</p>
		<ul>
			<li><i>File</i>: loads a document expressed according to model of <a href="http://alignapi.gforge.inria.fr/">INRIA's Alignment API</a>;</li>
			<li><i>Remote Alignment System</i>: allows to run an alignment task exploiting an external alignment system. This task, when completed, returns a list of alignments found.</li>
		</ul>
		<p>
			In both the cases, the tool allows to validate the alignments which the results can then be re-exported in the same INRIA's format and/or transformed into plain OWL/SKOS mapping triples and loaded into the project's dataset.
		</p>

		<img class="screenshot" src="images/alignmentvalidation-page.png" alt="alignment_validation_menu" />

		<p>
			<i>
				Note: the alignment from remote system has been introduced in the version 6.1.0. The figures present in the next section are taken from an old version of VocBench where the only alignment from file was available, so they might slightly differ from the actual UI, but the operation remained the same.
			</i>
		</p>

		<h2>Alignment from file</h2>

		<p>
			The UI of the tools is quite simple, it allows to select and load an alignment file stored into the filesystem. This tools can handle <i>rdf/xml</i> alignment files compliant with <a href="http://alignapi.gforge.inria.fr/format.html">INRIA Alignment format</a>.
		</p>
		<img class="screenshot" src="images/alignmentvalidation-listing.png" alt="alignment_validation_layout" />
		<p>
			The tool is able to recognize, through the baseURI of the two aligned ontologies, which one is the source ontology (the ontology currently loaded in VocBench) and which one is the target. If none of the baseURIs of the aligned ontologies corresponds to the baseURI of the local ontology, an error message will be prompted.
		</p>
		<p>The images in this page show the process of validating alignments between the Iasted ontology, loaded in VB, and the Sigkdd ontology. The ontologies and alignment files are downloadable from the <a href="http://oaei.ontologymatching.org/2013/conference/index.html">conference track of OAEI 2013</a>.</p>
		<p>
			Once the alignment file has been loaded, the tool shows a table with the alignments to be validated. Each row reports the source and target resources to align, the proposed alignment relation together with a measure representing the confidence of the alignment, and two buttons allowing the user to accept or reject the proposed alignment. Two further columns are automatically filled by the system:
		</p>
		<ul>
			<li><i>Mapping Property</i>. The cells in this column suggest a mapping property from the OWL or SKOS vocabularies to be used for the proposed alignment. This property depends on the mapping relation proposed for the alignment (e.g. a &quot;&gt;&quot; relation between two skos concepts would result in proposing a <code>skos:broaderMatch</code> mapping property among them) and is computed by the system only after an alignment is accepted. The property can then be modified by the user;</li>
			<li><i>Status</i>. This column reports, by means of an icon, the status of the alignment. There are three status marks: accepted, rejected and error. This last status is applied when the relation between the two aligned resources cannot be asserted for some reason (for instance, due to the types of the involved resources and the mapping property being proposed, e.g. two classes related with <i>hasInstance</i>).</li>
		</ul>
		<p>The user can choose to accept or reject the proposed alignments one by one, or apply some quick actions. </p>
		<div class="figure"><img src="images/alignmentvalidation-quickactions.png" alt="alignment_validation_actions" /></div>
		<p>Available quick actions are: </p>
		<ul>
			<li><i>Accept all</i>: mark as accepted all the alignments;</li>
			<li><i>Reject all</i>: mark as rejected all the alignments;</li>
			<li><i>Accept all above the threshold</i>: mark as accepted all the alignments with the relation measure higher than the given threshold;</li>
			<li><i>Reject all under the threshold</i>: mark as rejected all the alignments with the relation measure lower than the given threshold;</li>
		</ul>


		<p>
			This tool also allows user to navigate through the alignments using the up/down keys, and also to accept and reject them respectively with A and R keys.
		</p>


		<p>As explained before, accepting an alignment could result in an error message if the relation between the aligned resource cannot be asserted.
		</p>
		<div class="figure"><img src="images/alignmentvalidation-invalidrelation.png" alt="alignment_validation_actions" /></div>


		<p>
			The tool allows users to customize two validation aspects in a sequential order: by first, the alignment relation can be changed among the set of relations available in the INRIA's Alignment format, the mapping property can then be selected accordingly (i.e. the mapping property must implement the relation asserted in the INRIA's format and be compatible with the types of the aligned resources).
		</p>
		<div class="figure"><img src="images/alignmentvalidation-accepting.png" alt="alignment_validation_actions" /></div>
		<p>
			The changes brought by the user can be exported to a new RDF file in order to save a snapshot of the validation process and reload it at a later time.
			Note that the exported alignment file now complements the Alignment format with added custom information: the <i>mappingProperty</i> and the <i>status</i> attributes, detailing the information specified by the user during the validation process.</p>

		<p>As an alternative to the export, the user can project the validated relationships into plain mapping triples (adopting the standard mapping properties identified in the &quot;Mapping Property&quot; column) and assert them into the dataset being edited in the current project, by simply depressing the <i>Apply to the ontology</i> button. Then the tool adds to and removes from the ontology the triples corresponding respectively to the accepted and rejected alignments. A report is then prompted to the user, as in the following figure.</p>
		<p>
			The tool offers a few configuration options available through the Settings (figure below). The Settings can be accessed through the "gear" button on top of the alignment view. Through the Settings, the user can change the relation meter aspect (<i>Relation</i> column), the maximum amount of alignments shown and the action to perform on the rejected alignments when they are applied to the working ontology.
		</p>
		<div class="figure"><img src="images/alignmentvalidation-settings.png" alt="alignment_validation_actions" /></div>

		<h2>Alignment from Remote Alignment System</h2>

		<p>
			The alignment from Remote Alignment System, as the name suggests, exploits an external system in order to find and generate potential matching between resources of two different datasets.
			Even in this case the UI of the tool is initially quite simple: a <i>Tasks</i> panel lists the alignment task running or completed that involve the current project (the left one). For each task it is shown the name of the left and right datasets, the status of the alignment task, that can be <i>Completed</i> as in the figure, <i>Error</i> in case of problem during the alignment task or it can show a percentage of completion of the process, and the time when the process started and eventually ended (if completed). Finally, when the task is completed, an <i>edit alignment</i> button is available in order to show and manage the produced alignments.
		</p>

		<div class="figure"><img src="images/alignmentvalidation-remote-system.png" alt="alignment remote system" /></div>

		<p>
			If no alignment service is associated with the project, the following dialog will inform the user about that. An administrator or a project manager should associate one, by clicking on the <i>gear</i> button in the hade of the <i>Tasks</i> panel.
		</p>

		<div class="figure"><img src="images/alignmentvalidation-no-remote-service.png" alt="no alignment service" /></div>

		<p>
			From the toolbar located in the <i>Tasks</i> panel header it is possible to create new alignment tasks (through the <i>plus</i> button), to remove an existing one (through the <i>minus</i> button) and to refresh the list of tasks, the latter is especially useful in order to check the update of running tasks.
		</p>

		<p>
			The creation of a new alignment task asks for two projects, the left and the right. The left one is always the current open project and cannot be changed, the right one can be chosen among the open projects for which the current one has <i>READ</i> authorizations (see <a href="project_access_control.jsf">here</a> for details).
			Only in case of Edoal project, both the left and right datasets are constrained to the projects involved already in the Edoal one.
		</p>

		<div class="figure"><img src="images/alignmentvalidation-newtask.png" alt="alignment remote new task" /></div>

		<p>
			An alignment task, in order to work properly, requires that the two projects involved have been profiled, namely that the metadata about the datasets have been discovered. The icons at the end of the <i>Left project</i> and <i>Right project</i> rows in the <i>Create task</i> dialog, show the statuses of these metadata: a "v" tells that the project has been successfully profiled, and so its metadata are available, while an "x" tells that no metadata were found and so the project needs to be profiled. The profilation of the projects can be run (event if already done, the new metadata will simply overwrite the old one) directly from this dialog using the buttons with the "bar chart" icon. Only when both the projects are selected and the metadata are discovered, the <i>Profile</i> button is enabled. When the user clicks on this button, VocBench will use <a href="http://art.uniroma2.it/maple">MAPLE</a> to produce a report describing the given alignment scenario, which offers several options for matching strategies. By leveraging such scenario description, the user can (optionally) define an alignment plan, and then click the button "OK" to start a new task. See the page on <a href="alignment_systems.jsf">alignment systems</a> for further details.
		</p>

		<p>
			The following figure depicts the dialog for choosing an alignment service.
		</p>

		<div class="figure"><img src="images/alignmentvalidation-choose-service.png" alt="alignment remote choose service" /></div>

		<p>
			Clicking on the "wrench" button reveals the following dialog for adding/removing service configurations.
		</p>

		<div class="figure"><img src="images/alignmentvalidation-add-service.png" alt="alignment remote add service" /></div>

		<p>
			Choosing an alignment service binds it to the current project only. Alternatively, a service can be made a <em>default</em> choice for any project: this service will be used in any project that hasn't been associated explicitly to an alignment service.
		</p>

		<p>
			A completed alignment task can be finally edited though the <i>edit alignment</i> button. The click on this button load the alignments discovered by the task in an <i>Alignments</i> panel below. This panel is the same already described for the alignment from file, so refer to the previous section for details about the available operations.
		</p>

		<div class="figure"><img src="images/alignmentvalidation-remote-edit.png" alt="alignment remote edit" /></div>

		<h2>FAQ</h2>
		<ul>
			<li><strong>Q</strong>: <i>Despite the baseURI of the ontology loaded in ST matches the baseURI of one of the aligned ontology in the alignment file, the tool fails to load the alignment file with the error "Alignment file loading error: Failed to open and validate the given alignment file. None of the two aligned ontologies matches the current project ontology"<br />
				</i><strong>A</strong>: <i>Probably the alignment file contains an error. The most common is a missing # at the end of the Alignment namespace, e.g. http://knowledgeweb.semanticweb.org/heterogeneity/alignment#</i></li>
		</ul>
		<ul>
			<li><strong>Q</strong>: <i>The tool fails to load the alignment file with the error "STException: java.io.IOException: org.openrdf.rio.RDFParseException: unqualified attribute '...' not allowed"<br />
				</i><strong>A</strong>: <i>Probably the alignment file is an XML file, not RDF. INRIA's Alignment API allow for the possibility to generate XML Alignment files which are however not valid RDF.</i></li>
		</ul>



	</ui:define>
</body>

</html>